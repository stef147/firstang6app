import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'first-ang-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  encapsulation: ViewEncapsulation.Emulated,
})

export class AppComponent   {

  title: string;

  @Input() name = 'default name';

  @Output() message = new EventEmitter<any>(name);

  constructor() {
    this.title =  'Super duper Angular 6 app!!!';
  }

  onClick() {
    console.log('onclick called' + this.name);
    this.message.emit(this.name);
  }
}
