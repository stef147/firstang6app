//Install express server
const express = require('express');
var compression = require('compression');

const app = express();
app.use(compression());

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/firstAng6App'));


console.log("Starting server on port 5201");
app.listen(5201,"0.0.0.0");
